# Scrap to book :
    Ce programme permet d'extraire du site http://books.toscrape.com/  les informations tarifaires sur les livres de différentes categories proposées et des les stockers dans un fichier csv. Il recupère aussi l'image du ou des livres scrappés.
    Il peut etre utilisé sur un livre, une categorie ou l'ensemble du site.
    Les fichiers seront stockés dans un dossier data.

---

 ## Pré-requis 
   Nous utiliserons python3 pour ce programme ainsi que les librairies:
   - requests
   - BeautifulSoup4

---

 ## installation 
 1. Dans un terminal entrez les commandes:
   - ``git clone https://graquil@bitbucket.org/SensofLife/oc_p2.git``
   - ``cd oc_p2``

1. Mise en place de l'environnement virtuel:
   1. sous Windows:
      - ``python -m venv env``
      - ``source env/scripts/activate``
   
   2. sous Unix:
      - ``python3 -m venv env``
      - ``source env/bin/activate``

2. ```pip install -r requirement.txt``` 
---

## Lancement du programme

- **Pour scrapper un livre**

1. sous windows
    - ``` python scrap_book.py <url_du_livre>```
2. sous UNIX
   - ```python3 scrap_book.py <url_du_livre>```

- **Pour scrapper une categorie**
1. sous windows
    - ``` python scrap_category.py <url_de_la category>```
2. sous UNIX
   - ```python3 scrap_category.py <url_de_la category>```

- **Pour scrapper le site**
1. sous windows
    - ``` python scrap_category.py <url_de_la category>```
2. sous UNIX
   - ```python3 scrap_category.py <url_de_la category>```



