"""
    Module pour le scrapping d'une catégorie du site http://books.toscrape.com/.
    Extrait les informations ainsi que les images de chaque livres d'une catégorie
    les enregistre dans un dossier data avec un fichier CSV(pour les info) 
    et dans un dossier(pour les images) portant le nom de la catégorie.

"""

import argparse
import os

import requests
from bs4 import BeautifulSoup

from scrap_book import scrap_book, create_csv_file, download_image


def scrap_category(url):
    """ Scrape une categorie du site http://books.toscrape.com/.
    :param a(str):
        url de la catégorie a scrapper

    :rtype a(list):
        liste des urls des livres d'une catégorie
    """
    urls_books_category = []
    while True:
        try:
            r = requests.get(url)
            r.encoding = "utf-8"
            if r.ok:
                soup = BeautifulSoup(r.text, features="html.parser")
                for a in soup.select(".image_container a"):
                    urls_books_category.append(a["href"].replace("../../..", "https://books.toscrape.com/catalogue"))
                
                next_page_a = soup.select_one(".next>a")
                if not next_page_a:
                    break
                separate_url = url.split("/")
                separate_url.pop(-1)
                separate_url.append(next_page_a["href"])
                url = "/".join(separate_url)
    
        except requests.exceptions.ConnectionError:
            raise Exception("url incorrect")
        except requests.exceptions.MissingSchema:
            raise Exception("veuillez entrer un url au format http://<url>.com")

    return urls_books_category


def main(url):
    """
        Fonction principale, enregistre les données dans un fichier csv
    et les images dans un dossier du même nom que la categorie.

    :param a(str):
        url de la catégorie
    """

    for url in scrap_category(url):
        info_book = scrap_book(url)

        filename = os.path.join("data", info_book["category"])
        if not os.path.exists(filename):
            os.makedirs(filename)
        print(create_csv_file(filename, info_book))
        try:
            filename_book = os.path.join(filename, url.split("/")[-2])
            download_image(filename_book, info_book["image_url"])
        except OSError:
            filename_book = os.path.join(filename, info_book["title"].split(":")[0].replace(" ", "-"))
            download_image(filename_book, info_book["image_url"])
        

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()
    parser.add_argument("url", help="entrez l'url de la page de la categorie a scrapper")
    args = parser.parse_args()

    main(args.url)