"""
    Module pour le scrapping du site http://books.toscrape.com/.
    Permet d'extraire les informations d'un livre du site,
    de les tranformer et enregistrer en format csv avec
    l'image du livre en jpg dans le dossier data.
"""

import argparse
import csv
import re
import os
import urllib.request

import requests
from bs4 import BeautifulSoup

rating = {
            "One": "1",
            "Two": "2",
            "Three": "3",
            "Four": "4",
            "Five": "5"
}


def scrap_book(url):
    """Scrape la page d'un livre sur le site http://books.toscrape.com/.

    param a(str): 
        url du livre a visiter

    return(dict):
        Les informations du livre (nom, prix, dispo, etc...)
    """

    try:
        r = requests.get(url)
        r.encoding = "utf8"
        if r.ok:
            soup = BeautifulSoup(r.text, features="html.parser")
            title = soup.select_one("h1").text

            upc = soup.select_one(".table-striped>tr:nth-child(1)>td").text
            price_et = soup.select_one(".table-striped>tr:nth-child(3)>td").text

            price_it = soup.select_one(".table-striped>tr:nth-child(4)>td").text
            
            availability = soup.select_one(".table-striped>tr:nth-child(6)>td").text
            availability_nbr = re.search("\\d+", availability).group()
        
            description = soup.select_one("head>meta:nth-child(4)")["content"].strip()
        
            category = soup.select_one(".breadcrumb>li:nth-child(3)>a").text
        
            review_rating = soup.select_one(".star-rating")["class"][1]
            review_rating_nbr = rating[review_rating]
        
            image_url = soup.select_one(".item.active>img")["src"].replace("../..", "https://books.toscrape.com")
            
            return {
                "url": url,
                "title": title,
                "upc": upc,
                "price_et": price_et,
                "price_it": price_it,
                "availability": availability_nbr,
                "description": description,
                "category": category,
                "review_rating": review_rating_nbr,
                "image_url": image_url

            }
    except requests.exceptions.ConnectionError:
        raise Exception("url incorrect")
    except requests.exceptions.MissingSchema:
        raise Exception("veuillez entrer un url au format http://<url>.com")


def create_csv_file(filename, info):
    """permet de creer le fichier csv du livre
    
    :param a(str): 
        chemin du fichier a creer sans son extension
    
    :param b(dict):
        information du livre récupérée par la fonction scrap_book

    :return(str):
        nom du livre traité
    """
    
    file_path = filename + ".csv"

    info_book = info
    header = [
        "url", "title", "upc", "price_et", "price_it", "availability",
        "description", "category", "review_rating", "image_url"
        ]
    if not os.path.exists(file_path):
        with open(file_path, "w", newline="", encoding="utf-8-sig") as f:
            csvfile = csv.DictWriter(f, fieldnames=header, delimiter=",")
            csvfile.writeheader()

    with open(file_path, "a", newline="", encoding="utf-8-sig") as f:
        csvfile = csv.DictWriter(f, fieldnames=header, delimiter=",")
        csvfile.writerow(info_book)
    return info_book["title"]


def download_image(filename, image_url):
    """telécharge l'image du livre

    :param a(str): 
        chemin de l'image a créer sans l'extension

    :param b(str): 
        url de l'image a télécharger
    """
    try:
        file_path = filename + ".jpg"
        urllib.request.urlretrieve(image_url, file_path)
    except ValueError:
        raise Exception("url de l'image n'est pas correcte")


def main(url):
    """
      fonction principale, enregistre les données
    dans un fichier csv et l'image du livre

    :param a(str):
        url du livre a scrapper
    """
    if not os.path.exists("data"):
        os.makedirs("data")

    info_book = scrap_book(url)
    try:
        filename = os.path.join("data", url.split("/")[-2])
        print(create_csv_file(filename, info_book))
        download_image(filename, info_book["image_url"])
    except OSError:
        filename = os.path.join("data", info_book["title"].split(":")[0].replace(" ", "-")) 
        print(create_csv_file(filename, info_book))
        download_image(filename, info_book["image_url"])


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("url", help="entrez l'url de la page du livre a scrapper")
    args = parser.parse_args()

    main(args.url)

    