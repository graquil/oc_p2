"""
    Module pour le scrapping du site http://books.toscrape.com/.
    Extrait les informations de l'ensemble des livre du site,
    creait un fichier csv et un dossier, contenant les images des livres
    par catégorie dans un dossier data.
"""

import os

import requests
from bs4 import BeautifulSoup

from scrap_category import scrap_category
from scrap_book import scrap_book, create_csv_file, download_image

# constantes
URL = " http://books.toscrape.com/"


def scrap_home():
    """ scrape le site http://books.toscrape.com/. 
        :rtype(list):
            liste des urls des catégories
    """
    try:
        r = requests.get(URL)
       
        r.encoding = "utf-8"
        if r.ok:
            soup = BeautifulSoup(r.text, features="html.parser")
            return [URL + a["href"] for a in soup.select(".nav-list a")][1:]
    except requests.exceptions.ConnectionError:
        raise Exception("url incorrect")
    except requests.exceptions.MissingSchema:
        raise Exception("veuillez entrer un url au format http://<url>.com")


def main():
    """
        enregistre les données et les images dans un fichier csv par categegorie
    """
    for urls_category in scrap_home():
        for url_book in scrap_category(urls_category):
            info_book = scrap_book(url_book)

            filename = os.path.join("data", info_book["category"])
            if not os.path.exists(filename):
                os.makedirs(filename)

            print(create_csv_file(filename, info_book))
            try:
                filename_book = os.path.join(filename, url_book.split("/")[-2])
                download_image(filename_book, info_book["image_url"])
            except OSError:
                filename_book = os.path.join(filename, info_book["title"].split(":")[0].replace(" ", "-"))
                download_image(filename_book, info_book["image_url"])
            

if __name__ == "__main__":
    main()